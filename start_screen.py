"""
Starting game screen and its tools
"""
import pygame

from assets import get_image


class StartScreen:
    """
    Represents a starting game screen
    Able to be drawn, processes events and updates by itself
    """

    def __init__(self, surface, background_id):
        """
        Construct a starting screen
        Will be drawn at given surface, with given background
        """
        self.background_image = get_image(background_id)

        self.exit_button = pygame.Rect(100, 370, 240, 50)
        self.authors_button = pygame.Rect(100, 300, 240, 50)
        self.settings_button = pygame.Rect(100, 230, 240, 50)
        self.play_button = pygame.Rect(370, 230, 170, 190)

        self.changing_to = False

    def draw(self, surface):
        """Draw screen"""
        # draw background
        surface.blit(self.background_image, (0, 0))
        # draw buttons
        pygame.draw.rect(surface, pygame.Color(
            255, 0, 0), self.exit_button)
        pygame.draw.rect(surface, pygame.Color(
            0, 255, 0), self.authors_button)
        pygame.draw.rect(surface, pygame.Color(
            0, 0, 255), self.settings_button)
        pygame.draw.rect(surface, pygame.Color(
            255, 255, 0), self.play_button)

    def process_events(self):
        """Process events relevant to this screen"""
        from main import change_screen, end_game
        for e in pygame.event.get():
            if e.type == pygame.MOUSEBUTTONDOWN:
                if self.exit_button.collidepoint(*e.pos):
                    print('Exit button pressed')
                    end_game()
                elif self.authors_button.collidepoint(*e.pos):
                    print('Authors button pressed')
                    # TODO: show authors screen
                elif self.settings_button.collidepoint(*e.pos):
                    print('Settings button pressed')
                    # TODO: show settings screen
                elif self.play_button.collidepoint(*e.pos):
                    print('Play button pressed')
                    # TODO: load saved game/start it
                    change_screen('world')
            elif e.type == pygame.QUIT:
                end_game()

    def update(self, elapsed):
        """Update screen state"""
        pass
