"""
Contains tools for working with maps
"""
import pygame
import pytmx
from pytmx.util_pygame import load_pygame
import pyscroll

TILE_SIZE = 64


class Map:
    """
    Represents a game Map
    """

    def __init__(self, filename):
        """Load a map from file and construct it"""
        self.tmx_map = load_pygame(filename)
        self.map_data = pyscroll.data.TiledMapData(self.tmx_map)
        self.hardtiles = []     # areas that collide with characters

        # detect hard tiles (characters collide with those)
        for layer in self.tmx_map.layers:
            for x, y, gid in layer:
                if self.tmx_map.tile_properties[gid]['st_solid']:
                    self.hardtiles.append(
                        pygame.Rect(x*TILE_SIZE, y*TILE_SIZE, TILE_SIZE, TILE_SIZE))

    def get_colliders(self):
        """
        Returns a list representing map collision box
        """
        return self.hardtiles
