"""
World screen and its tools
"""
import pygame
import pyscroll

from player import Player, PLAYER_WIDTH, PLAYER_HEIGHT
from assets import get_map


class WorldScreen:
    """
    Represents main game screen - the world one
    """

    def __init__(self, surface, map_id):
        """
        Construct world screen with given surface to draw and a map
        """
        self.map = get_map(map_id)
        self.player = Player(1200, 1200)
        self.player_direction = [0, 0]
        w, h = surface.get_size()

        self.renderer = pyscroll.BufferedRenderer(
            self.map.map_data, (w, h), clamp_camera=True)
        self.group = pyscroll.PyscrollGroup(map_layer=self.renderer)

        self.group.add(self.player)

        self.fullscreen = False

    def draw(self, surface):
        """Draw screen"""
        self.group.center(self.player.rect.center)
        self.group.draw(surface)

    def process_events(self):
        """Process events relevant to this screen"""
        from main import end_game, set_fullscreen
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                end_game()
            elif e.type == pygame.KEYDOWN:
                if e.key == pygame.K_ESCAPE:
                    end_game()
                elif e.key == pygame.K_a:
                    self.player_direction[0] = -1
                elif e.key == pygame.K_w:
                    self.player_direction[1] = 1
                elif e.key == pygame.K_s:
                    self.player_direction[1] = -1
                elif e.key == pygame.K_d:
                    self.player_direction[0] = 1
                elif e.key == pygame.K_F11:
                    self.fullscreen = not self.fullscreen
                    set_fullscreen(self.fullscreen)
            elif e.type == pygame.KEYUP:
                if e.key in (pygame.K_a, pygame.K_d):
                    self.player_direction[0] = 0
                elif e.key in (pygame.K_w, pygame.K_s):
                    self.player_direction[1] = 0
            elif e.type == pygame.VIDEORESIZE:
                self.renderer = pyscroll.BufferedRenderer(
                    self.map.map_data, (e.w, e.h), clamp_camera=True)
                self.group = pyscroll.PyscrollGroup(map_layer=self.renderer)
                self.group.add(self.player)

    def update(self, elapsed):
        """Update state"""
        self.group.update(elapsed, self.player_direction,
                          self.map.get_colliders())
        self.group.remove(self.player)
        self.group.add(self.player)
