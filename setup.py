import sys

from cx_Freeze import setup, Executable

options = {
    'build_exe': {
        'includes': [
            'assets',
            'map',
            'player_model',
            'player',
            'start_screen',
            'world_screen'
        ],
	'excludes': [
	    'tkinter'
	],
        'packages': [
            'pygame',
            'pytmx',
            'pyscroll'
        ],
        'include_files': [
            'assets'
        ]
    }
}

executables = [
    Executable('main.py')
]

setup(name='Stalevar',
      version='alpha1',
      description='Stalevar game',
      options=options,
      executables=executables)
