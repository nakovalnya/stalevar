"""
A wrapper for easier loading and caching of images and maps
"""
import os

import pygame

import map

__all__ = ["get_image", "get_map"]

images = {}
maps = {}


def get_image(image_id):
    """
    Given an image string id, returns its pygame.Surface
    Performs image caching
    """
    if image_id not in images:      # loading and caching
        filename = os.path.join("assets", image_id + ".png")
        print("Loading {} image as {}..".format(image_id, filename))
        images[image_id] = pygame.image.load(filename).convert_alpha()
    return images[image_id]


def get_map(map_id):
    """
    Given a map id, returns it as map.Map class instance
    Performs map caching
    """
    if map_id not in maps:
        filename = os.path.join("assets", map_id + ".tmx")
        maps[map_id] = map.Map(filename)
    return maps[map_id]
