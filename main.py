"""
A starting point of the game
Manages main loop and screen changing
"""
import pygame

import start_screen
import world_screen


def process_events():
    """Process events"""
    screen.process_events()


def update(elapsed):
    """Update game state"""
    screen.update(elapsed)


def draw(surface):
    """Main draw"""
    # window.fill((0, 0, 0))
    screen.draw(surface)
    pygame.display.flip()


def change_screen(newscreen):
    """Change current screen to another given its string name"""
    global screen
    print(f'Changing screen to {newscreen}...')
    if newscreen == 'start':
        screen = start_screen
    elif newscreen == 'world':
        screen = world_screen


def end_game():
    # TODO: save game state
    pygame.quit()
    exit()


def set_fullscreen(fullscreen):
    global window
    # pygame.display.quit()
    # pygame.display.init()
    if fullscreen:
        window = pygame.display.set_mode(
            (0, 0), pygame.RESIZABLE | pygame.HWSURFACE | pygame.DOUBLEBUF)
    else:
        window = pygame.display.set_mode(
            (0, 0), pygame.RESIZABLE | pygame.HWSURFACE | pygame.DOUBLEBUF | pygame.FULLSCREEN)


pygame.init()
pygame.font.init()
pygame.display.init()

window = pygame.display.set_mode(
    (0, 0), pygame.RESIZABLE | pygame.HWSURFACE | pygame.DOUBLEBUF)
pygame.display.set_caption('Stalevar')

start_screen = start_screen.StartScreen(window, 'start_screen_background')
world_screen = world_screen.WorldScreen(window, 'world')  # map name

screen = start_screen

playing = True

clock = pygame.time.Clock()

while playing:
    elapsed = clock.tick(60)
    #print(f'Ticked in {elapsed} ms')
    process_events()
    update(elapsed)
    draw(window)
