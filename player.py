"""
A player in a game world
"""
import pygame


from assets import get_image

PLAYER_SPEED = 0.3  # px per ms
PLAYER_WIDTH = 88
PLAYER_HEIGHT = 128
HITBOX_COMPENSATION = 1


class Player(pygame.sprite.Sprite):
    """
    Manages player in a world screen:
    location, speed, animations and so on
    """

    def __init__(self, x, y):
        """
        Construct a player starting at specified position
        """
        pygame.sprite.Sprite.__init__(self)
        self.image = get_image('player')
        self._position = [x, y]
        self.speedx = 0
        self.speedy = 0

        self.rect = self.image.get_rect().move(x, y)
        self.colliderect = pygame.Rect(HITBOX_COMPENSATION,
                                       HITBOX_COMPENSATION,
                                       PLAYER_WIDTH - 2*HITBOX_COMPENSATION,
                                       PLAYER_HEIGHT - 2*HITBOX_COMPENSATION)

    def position(self):
        return list(self._position)

    def update(self, elapsed, direction, collide_with):
        """
        Update location, checking for collisions
        """
        # update speed by x axis
        if direction[0] == -1:
            self.speedx = -PLAYER_SPEED
        elif direction[0] == 1:
            self.speedx = PLAYER_SPEED
        else:
            self.speedx = 0

        # update speed by y axis
        if direction[1] == 1:
            self.speedy = -PLAYER_SPEED
        elif direction[1] == -1:
            self.speedy = PLAYER_SPEED
        else:
            self.speedy = 0

        # update textures
        player_texture = 'player'
        if direction[0] == -1:
            player_texture += '_left'
        elif direction[0] == 1:
            player_texture += '_right'

        if direction[1] == -1:
            player_texture += '_up'
        elif direction[1] == 1:
            player_texture += '_down'

        self.image = get_image(player_texture)

        nextx = self._position[0] + self.speedx * elapsed
        nexty = self._position[1] + self.speedy * elapsed

        can_move = [True, True]
        for obj in collide_with:
            if self.colliderect.move(nextx, self._position[1]).colliderect(obj):
                can_move[0] = False
            if self.colliderect.move(self._position[0], nexty).colliderect(obj):
                can_move[1] = False

        if can_move[0]:
            self._position[0] = nextx
        if can_move[1]:
            self._position[1] = nexty

        self.rect.topleft = self._position
